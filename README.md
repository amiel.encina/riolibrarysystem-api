# Rio's Library System API

This project is used for the api of rio's library system, the purpose of this is to handle requests from the front-end and then return the data back to the front-end. 

### Installing

*Before doing the steps below, make sure that you have xampp/mamp installed and running, and you have created a database with your desired database name*

STEP 1 Open the terminal then type:
```
composer install
```
STEP 2 Create a file called .env then copy the contents of the .env.example to your .env file. this is what the .env.example file looks like:
```
  APP_NAME=Laravel
  APP_ENV=local
  APP_KEY=
  APP_DEBUG=true
  APP_URL=http://localhost

  LOG_CHANNEL=stack

  DB_CONNECTION=mysql
  DB_HOST=127.0.0.1
  DB_PORT=3306
  DB_DATABASE=laravel
  DB_USERNAME=root
  DB_PASSWORD=

  BROADCAST_DRIVER=log
  CACHE_DRIVER=file
  QUEUE_CONNECTION=sync
  SESSION_DRIVER=file
  SESSION_LIFETIME=120

  REDIS_HOST=127.0.0.1
  REDIS_PASSWORD=null
  REDIS_PORT=6379

  MAIL_DRIVER=smtp
  MAIL_HOST=smtp.mailtrap.io
  MAIL_PORT=2525
  MAIL_USERNAME=null
  MAIL_PASSWORD=null
  MAIL_ENCRYPTION=null

  AWS_ACCESS_KEY_ID=
  AWS_SECRET_ACCESS_KEY=
  AWS_DEFAULT_REGION=us-east-1
  AWS_BUCKET=

  PUSHER_APP_ID=
  PUSHER_APP_KEY=
  PUSHER_APP_SECRET=
  PUSHER_APP_CLUSTER=mt1

  MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
  MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
STEP 3 Replace the values on your .env with the necessary values to be filled out in the .env for example:
```
  DB_CONNECTION=mysql
  DB_HOST=localhost
  DB_PORT=3306
  DB_DATABASE=NAME_OF_YOUR_DATABASE
  DB_USERNAME=USERNAME_IN_XAMPP/MAMP
  DB_PASSWORD=PASSWORD_IN_XAMPP/MAMP
```

STEP 4 In the terminal type:
```
  php artisan key:generate
```

STEP 5 after generating the key, type:
```
  php artisan migrate:refresh --seed
```

## Deployment

Open the terminal then type:
```
  php artisan serve
```

## Built With

* [Laravel](https://laravel.com/) - PHP Framework used for developing the API.



## Author

* **Amiel Encina** - *Developer*

