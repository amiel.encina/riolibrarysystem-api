<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $fillable = [
        'image_url', 'title', 'author', 'genre', 'section', 'status'
    ];

    public static function showAll() {
        try {
            $books =  Books::all();

            return response()->json($books, 200);
        } catch(Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    public static function findById($id) {
        $books =  Books::where('id', $id)->get();
        return $books;
    }

    public static function addBook($data) {
        try {
            $searchBook = Books::where('title', $data['title'])->get();
            if($searchBook->isNotEmpty()) {
                return response()->json('The book is already inside the library', 400);
            }
            if($data['image_url'] !== null) {
                $image_name = date('dmY') . '_' . request()->file('image_url')->getClientOriginalName();
                $path = public_path() . "/images/";
                file_put_contents($path, $image_name);
                $data['image_url'] = $image_name;
            }
            $books =  Books::create($data);

            return response()->json($books, 201);
        } catch (Exception $e) {

            return response()->json("Error: ".$e->getMessage(), 400);
        }
    }

    public static function updateBook($data, $id) {
        $books =  Books::where('id', $id)->update($data);

        return $books;
    }

    public static function deleteBook($id) {
        $books =  Books::where('id', $id)->delete();

        return $books;
    }
}
